/***************************** BEGIN LICENSE BLOCK ***************************

 Copyright (C) 2021 Sentinel, LLC. All Rights Reserved.

 ******************************* END LICENSE BLOCK ***************************/
package com.sentinel.impl.sensor.audio.ml.nlp.speech;

/**
 *
 * @author Nick Garay
 * @since Mar. 12, 2021
 */
public enum SpeechRecognizerType {
    LIVE,
    STREAM
}
