/***************************** BEGIN LICENSE BLOCK ***************************

 Copyright (C) 2020-2021 Nicolas Garay. All Rights Reserved.

 ******************************* END LICENSE BLOCK ***************************/
package com.sentinel.impl.sensor.pibot.searchlight;

import com.sentinel.impl.sensor.common.control.BaseSensorControl;
import net.opengis.swe.v20.DataBlock;
import net.opengis.swe.v20.DataRecord;
import org.sensorhub.api.command.CommandException;
import org.vast.swe.SWEHelper;

/**
 * Control specification and provider for PiBot SearchlightSensor Module
 *
 * @author Nick Garay
 * @since Jan. 24, 2021
 */
public class SearchlightControl extends BaseSensorControl<SearchlightSensor> {

    private static final String SENSOR_CONTROL_NAME = "SearchlightControl";

    protected SearchlightControl(SearchlightSensor parentSensor) {
        super(SENSOR_CONTROL_NAME, parentSensor);
    }

    @Override
    public boolean execCommand(DataBlock command) throws CommandException {

        try {

            DataRecord commandData = commandDataStruct.copy();
            commandData.setData(command);
            SearchlightState searchLightColor = SearchlightState.fromString(commandData.getData().getStringValue());
            parentSensor.setSearchlightState(searchLightColor);

        } catch (Exception e) {

            throw new CommandException("Failed to command the SearchlightSensor module: ", e);
        }

        return true;
    }

    @Override
    protected void init() {

        SWEHelper factory = new SWEHelper();
        commandDataStruct = factory.createRecord()
                .name(getName())
                .updatable(true)
                .definition(SWEHelper.getPropertyUri("SearchlightSensor"))
                .label("SearchlightSensor")
                .description("An RGB light, whose color is given by one of the color choices specified")
                .addField("Color",
                        factory.createCategory()
                                .name("RGB Color")
                                .label("RGB Color")
                                .definition(SWEHelper.getPropertyUri("Color"))
                                .description("The color state of the searchlight")
                                .addAllowedValues(
                                        SearchlightState.OFF.name(),
                                        SearchlightState.WHITE.name(),
                                        SearchlightState.RED.name(),
                                        SearchlightState.MAGENTA.name(),
                                        SearchlightState.BLUE.name(),
                                        SearchlightState.CYAN.name(),
                                        SearchlightState.GREEN.name(),
                                        SearchlightState.YELLOW.name())
                                .value(SearchlightState.OFF.name())
                                .build())
                .build();
    }
}
