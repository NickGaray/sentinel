/***************************** BEGIN LICENSE BLOCK ***************************

 Copyright (C) 2020-2021 Nicolas Garay. All Rights Reserved.

 ******************************* END LICENSE BLOCK ***************************/
package com.sentinel.impl.sensor.pibot.ultrasound;

import org.sensorhub.utils.OshBundleActivator;

public class Activator extends OshBundleActivator {
}
