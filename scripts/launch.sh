#!/bin/bash
java -Xmx2g -Dlogback.configurationFile=./logback.xml -cp "lib/*" -Djava.system.class.loader="org.sensorhub.utils.NativeClassLoader" org.sensorhub.impl.SensorHub config.json db
