FROM ubuntu:18.04 AS osh-deployment

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y openjdk-8-jdk git unzip curl nano certbot wget nginx

ARG version=''

# General system setup
RUN useradd -r -s /bin/false/ osh && echo "US/Central" > /etc/timezone

# Install OSH Node
#COPY --from=osh-build  ./osh-node-sentinel/build/distributions/osh-node-sentinel-$version.zip .
ADD build/distributions/osh-node-sentinel-$version.zip .
ADD container/osh-sentinel /etc/init.d/
RUN awk -v version=$version '{ if ( $0~"OSH_HOME=/opt/osh" ) { print $0"-"version } else { print $0 } }' /etc/init.d/osh-sentinel > /etc/init.d/osh-sentinel-versioned && \
    mv /etc/init.d/osh-sentinel-versioned /etc/init.d/osh-sentinel
RUN chmod 755 /etc/init.d/osh-sentinel
RUN unzip osh-node-sentinel-$version.zip -d /opt && \
    rm osh-node-sentinel-$version.zip && \
    chown -R osh:osh /opt/osh-node-sentinel* && \
    chmod 755 /opt/osh-node-sentinel-$version/launch.* && \
    update-rc.d osh-sentinel defaults && \
    update-rc.d osh-sentinel enable

# Configure nginx
ADD container/nginx-users container/nginx.crt container/nginx.key /etc/nginx/
ADD container/nginx-default /etc/nginx/sites-available/default
RUN echo "daemon off;" >> /etc/nginx/nginx.conf

# Open ports
EXPOSE 80 443

CMD service osh-sentinel start && \
    service nginx start