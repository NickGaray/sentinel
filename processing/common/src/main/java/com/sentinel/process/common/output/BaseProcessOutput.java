/***************************** BEGIN LICENSE BLOCK ***************************

 Copyright (C) 2020-2021 Nicolas Garay. All Rights Reserved.

 ******************************* END LICENSE BLOCK ***************************/
package com.sentinel.process.common.output;

import net.opengis.swe.v20.DataComponent;
import net.opengis.swe.v20.DataEncoding;
import org.sensorhub.api.data.IStreamingDataInterface;
import org.sensorhub.api.sensor.SensorException;
import org.slf4j.Logger;

/**
 * Base Process Output for Sentinel Smart Sensors
 *
 * @author Nick Garay
 * @since 1.0.0
 */
public abstract class BaseProcessOutput implements IStreamingDataInterface {

    protected Logger logger;

    protected static final int MAX_NUM_TIMING_SAMPLES = 10;

    protected int dataFrameCount = 0;

    protected final long[] timingHistogram = new long[MAX_NUM_TIMING_SAMPLES];

    protected final Object histogramLock = new Object();

    protected long lastDataFrameTimeMillis;

    protected DataComponent dataStruct;

    protected DataEncoding dataEncoding;

    public BaseProcessOutput() {

    }

    @Override
    public DataComponent getRecordDescription() {

        return dataStruct;
    }

    @Override
    public DataEncoding getRecommendedEncoding() {

        return dataEncoding;
    }

    @Override
    public double getAverageSamplingPeriod() {

        long accumulator = 0;

        synchronized (histogramLock) {

            for (int idx = 0; idx < MAX_NUM_TIMING_SAMPLES; ++idx) {

                accumulator += timingHistogram[idx];
            }
        }

        return accumulator / (double) MAX_NUM_TIMING_SAMPLES;
    }

    protected abstract void init() throws SensorException;
}