/***************************** BEGIN LICENSE BLOCK ***************************

 Copyright (C) 2020-2021 Nicolas Garay. All Rights Reserved.

 ******************************* END LICENSE BLOCK ***************************/
package com.sentinel.process.common.config;

import org.sensorhub.api.config.DisplayInfo;

/**
 * Configuration parameters for Video data
 *
 * @author Nick Garay
 * @since 1.0.0
 */
public class VideoParameters {

    @DisplayInfo.Required
    @DisplayInfo(label = "", desc = "Width of the video frames")
    public int videoFrameWidth = 1024;

    @DisplayInfo.Required
    @DisplayInfo(label = "", desc = "Height of the video frames")
    public int videoFrameHeight = 768;
}
