/***************************** BEGIN LICENSE BLOCK ***************************

 Copyright (C) 2020-2021 Nicolas Garay. All Rights Reserved.

 ******************************* END LICENSE BLOCK ***************************/
package com.sentinel.process.image;

import net.opengis.swe.v20.DataBlock;
import net.opengis.swe.v20.DataComponent;
import net.opengis.swe.v20.DataRecord;
import net.opengis.swe.v20.DataStream;
import org.bytedeco.javacpp.Loader;
import org.bytedeco.opencv.opencv_objdetect.CascadeClassifier;
import org.sensorhub.api.common.SensorHubException;
import org.sensorhub.api.data.DataEvent;
import org.sensorhub.api.processing.DataSourceConfig;
import org.sensorhub.api.processing.ProcessException;
import org.sensorhub.impl.processing.AbstractStreamProcess;
import org.sensorhub.impl.sensor.videocam.VideoCamHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vast.data.AbstractDataBlock;
import org.vast.data.DataBlockMixed;
import org.vast.process.DataQueue;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.*;

/**
 * Process for performing feature detection on images from a video source
 *
 * @author Nick Garay
 * @since 1.0.0
 */
public class FeatureDetectionProcess extends AbstractStreamProcess<FeatureDetectionConfig>
{
    protected static final Logger logger = LoggerFactory.getLogger(FeatureDetectionProcess.class);

    private static final String PROCESS_IMAGE_INPUT_NAME = "imageInput";

    private static final String PROCESS_LOCATION_INPUT_NAME = "locationInput";

    protected FeatureDetectionOutput featureDetectionOutput;

    protected DataComponent imageInput;

    protected DataRecord locationInput;

    protected DataQueue imageQueue;

    protected DataQueue locationQueue;

    protected ExecutorService threadPool;

    private final List<CascadeClassifier> classifiers = new ArrayList<>();

    private String locationText = null;

    @Override
    public void init(FeatureDetectionConfig config) throws SensorHubException {

        logger.debug("Initializing");

        super.init(config);

        loadClassifiers();

        VideoCamHelper factory = new VideoCamHelper();

        DataStream outputDef = factory.newVideoOutputMJPEG(PROCESS_IMAGE_INPUT_NAME,
                config.videoParameters.videoFrameWidth, config.videoParameters.videoFrameHeight);

        imageInput = outputDef.getElementType();

        inputs.put(imageInput.getName(), imageInput);

        locationInput = factory.createDataRecord()
                .name(PROCESS_LOCATION_INPUT_NAME)
                .addField("time",
                        factory.createTime().asSamplingTimeIsoUTC().build())
                .addField("location",
                        factory.createText().name(PROCESS_LOCATION_INPUT_NAME).build())
                .build();

        inputs.put(locationInput.getName(), locationInput);

        featureDetectionOutput = new FeatureDetectionOutput(this);

        featureDetectionOutput.init();

        addOutput(featureDetectionOutput);

        logger.debug("Initialized");
    }

    @Override
    protected void connectInput(String inputName, String dataPath, DataQueue inputQueue) throws ProcessException {

        logger.debug("Connecting input");

        super.connectInput(inputName, dataPath, inputQueue);

        if (inputName.equals(imageInput.getName())) {

            logger.debug("Connecting image input");

            imageQueue = inputQueue;

        } else if (inputName.equals(locationInput.getName())) {

            logger.debug("Connecting location input");

            locationQueue = inputQueue;
        }

        logger.debug("Input connected ");
    }

    @Override
    public void start() throws SensorHubException {

        logger.debug("Starting");

        super.start();

        threadPool = new ThreadPoolExecutor(1, 1, 10000L,
                TimeUnit.SECONDS, new SynchronousQueue<>());

        logger.debug("Started");
    }

    @Override
    public void stop() {

        super.stop();
    }

    @Override
    protected synchronized void process(final DataEvent lastEvent) throws ProcessException {

        logger.debug("Processing event");

        try {

            byte[] imageData = null;

            double timeStamp = 0.0;

            if ((null != imageQueue) && imageQueue.isDataAvailable()) {

                DataBlock dataBlock = imageQueue.get();

                timeStamp = dataBlock.getDoubleValue(0) * 1000.0;

                AbstractDataBlock frameData = ((DataBlockMixed) dataBlock).getUnderlyingObject()[1];

                imageData = (byte[]) frameData.getUnderlyingObject();

            } else if ((null != locationQueue) && locationQueue.isDataAvailable()) {

                DataBlock dataBlock = locationQueue.get();

                locationText = dataBlock.getStringValue(1);
            }

            if (null != imageData) {

                Date date = new Date((long) timeStamp);

                SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");

                StringBuilder labelBuilder = new StringBuilder();

                labelBuilder.append(dateFormatter.format(date));

                if (null != locationText) {

                    labelBuilder.append(" ").append(locationText);
                }

                threadPool.execute(new FeatureDetector(classifiers, featureDetectionOutput,
                        imageData, labelBuilder.toString(), timeStamp));
            }

        } catch (InterruptedException e) {

            Thread.currentThread().interrupt();

        } catch (RejectedExecutionException e) {

            logger.warn("Exception while processing event, {}", e.toString());
        }

        logger.debug("Processed event");
    }

    @Override
    public boolean isPauseSupported() {

        return false;
    }

    @Override
    public boolean isCompatibleDataSource(DataSourceConfig dataSource) {

        return true;
    }

    private void loadClassifiers() {

        logger.debug("Loading classifiers");

        classifiers.clear();

        for (FeaturesEnum feature : getConfiguration().featureDetection.features) {

            URL resourceUrl = getClass().getResource(feature.getResource());

            File resourceFile;

            try {

                resourceFile = Loader.cacheResource(resourceUrl);

                String path = resourceFile.getAbsolutePath();

                classifiers.add(new CascadeClassifier(path));

            } catch (IOException e) {

                logger.error("Exception while loading classifiers, {}", e.toString());
            }
        }

        logger.debug("Classifiers loaded");
    }
}
