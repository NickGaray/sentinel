/***************************** BEGIN LICENSE BLOCK ***************************

 Copyright (C) 2020-2021 Nicolas Garay. All Rights Reserved.

 ******************************* END LICENSE BLOCK ***************************/
package com.sentinel.process.image;

import org.sensorhub.api.module.IModule;
import org.sensorhub.api.module.IModuleProvider;
import org.sensorhub.api.module.ModuleConfig;
import org.sensorhub.impl.module.JarModuleProvider;

/**
 * Descriptor for the {@link FeatureDetectionProcess}
 *
 * @author Nick Garay
 * @since 1.0.0
 */
public class FeatureDetectionDescriptor extends JarModuleProvider implements IModuleProvider
{
    @Override
    public Class<? extends IModule<?>> getModuleClass()
    {
        return FeatureDetectionProcess.class;
    }

    @Override
    public Class<? extends ModuleConfig> getModuleConfigClass()
    {
        return FeatureDetectionConfig.class;
    }
}
