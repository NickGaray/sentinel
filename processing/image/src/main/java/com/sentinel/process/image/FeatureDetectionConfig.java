/***************************** BEGIN LICENSE BLOCK ***************************

 Copyright (C) 2020-2021 Nicolas Garay. All Rights Reserved.

 ******************************* END LICENSE BLOCK ***************************/
package com.sentinel.process.image;

import com.sentinel.process.common.config.VideoParameters;
import org.sensorhub.api.config.DisplayInfo;
import org.sensorhub.api.processing.ProcessConfig;

import java.util.ArrayList;
import java.util.List;

/**
 * Configuration for feature detection in image data
 *
 * @author Nick Garay
 * @since 1.0.0
 */
public class FeatureDetectionConfig extends StreamProcessConfig {

    @DisplayInfo(label = "Video Parameters", desc = "Parameters used in configuring video elements")
    public VideoParameters videoParameters = new VideoParameters();

    @DisplayInfo(label = "Feature Detection Parameters", desc = "The set of features to be identified in an image frame")
    public FeatureDetection featureDetection = new FeatureDetection();

    protected class FeatureDetection {

        @DisplayInfo(label = "Features", desc = "The set of features to be identified in an image frame")
        public List<FeaturesEnum> features = new ArrayList<>();
    }
}
