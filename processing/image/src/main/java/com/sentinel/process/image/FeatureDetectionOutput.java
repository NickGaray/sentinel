/***************************** BEGIN LICENSE BLOCK ***************************

 Copyright (C) 2020-2021 Nicolas Garay. All Rights Reserved.

 ******************************* END LICENSE BLOCK ***************************/
package com.sentinel.process.image;

import com.sentinel.process.common.output.BaseProcessOutput;
import net.opengis.swe.v20.DataBlock;
import net.opengis.swe.v20.DataComponent;
import net.opengis.swe.v20.DataEncoding;
import net.opengis.swe.v20.DataStream;
import org.sensorhub.api.data.DataEvent;
import org.sensorhub.api.data.IDataProducerModule;
import org.sensorhub.api.event.IEventHandler;
import org.sensorhub.api.sensor.SensorException;
import org.sensorhub.impl.SensorHub;
import org.sensorhub.impl.sensor.videocam.VideoCamHelper;
import org.slf4j.LoggerFactory;
import org.vast.data.AbstractDataBlock;
import org.vast.data.DataBlockMixed;

/**
 * Packages and publishes an image that has had feature detection processing performed according to the
 * {@link FeatureDetector}
 *
 * @author Nick Garay
 * @since 1.0.0
 */
public class FeatureDetectionOutput extends BaseProcessOutput {

    private static final String PROCESS_OUTPUT_NAME = "FeatureDetectionOutput";

    private static final String PROCESS_OUTPUT_LABEL = "Image Detection Output";

    private static final String PROCESS_OUTPUT_DESCRIPTION =
            "Processed Video Feed from Sentinel Smart Cam used in Feature Detection";

    protected static final int MAX_NUM_TIMING_SAMPLES = 10;

    private final FeatureDetectionProcess parentProcess;

    private IEventHandler eventHandler;

    private long latestRecordTime = Long.MIN_VALUE;

    private DataBlock latestRecord;

//    private final Queue<FeatureDetector> processingQueue = new LinkedList<>();

    public FeatureDetectionOutput(FeatureDetectionProcess parentProcess) {

        logger = LoggerFactory.getLogger(FeatureDetectionOutput.class);

        logger.debug("Creating");

        this.parentProcess = parentProcess;

        logger.debug("Created");
    }

    protected synchronized void sendOutput(byte[] imageData, double timestamp) {

        logger.debug("Publishing processed image");

        // Update the timing histogram, used to compute average sampling period
        synchronized (histogramLock) {

            int dataFrameIndex = dataFrameCount % MAX_NUM_TIMING_SAMPLES;

            // Get a sampling time for latest set based on previous set sampling time
            timingHistogram[dataFrameIndex] = System.currentTimeMillis() - lastDataFrameTimeMillis;

            // Set latest sampling time to now
            lastDataFrameTimeMillis = timingHistogram[dataFrameIndex];
        }

        DataBlock dataBlock;

        if (latestRecord == null) {

            dataBlock = dataStruct.createDataBlock();

        } else {

            dataBlock = latestRecord.renew();
        }

        double sampleTime = System.currentTimeMillis() / 1000.0;

        if (timestamp > 0.0) {

            sampleTime = timestamp / 1000.0;
        }

        dataBlock.setDoubleValue(0, sampleTime);

        // Set underlying video frame data
        AbstractDataBlock frameData = ((DataBlockMixed) dataBlock).getUnderlyingObject()[1];

        frameData.setUnderlyingObject(imageData);

        latestRecord = dataBlock;

        latestRecordTime = System.currentTimeMillis();

        eventHandler.publish(new DataEvent(latestRecordTime, FeatureDetectionOutput.this, dataBlock));

        logger.debug("published processed image");
    }

    @Override
    public IDataProducerModule<?> getParentModule() {
        return parentProcess;
    }

    @Override
    public String getName() {

        return PROCESS_OUTPUT_NAME;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public DataComponent getRecordDescription() {
        return dataStruct;
    }

    @Override
    public DataEncoding getRecommendedEncoding() {
        return dataEncoding;
    }

    @Override
    public DataBlock getLatestRecord() {
        return latestRecord;
    }

    @Override
    public long getLatestRecordTime() {
        return latestRecordTime;
    }

    @Override
    protected void init() throws SensorException {

        logger.debug("Initializing");

        lastDataFrameTimeMillis = System.currentTimeMillis();

        int videoFrameHeight = parentProcess.getConfiguration().videoParameters.videoFrameHeight;

        int videoFrameWidth = parentProcess.getConfiguration().videoParameters.videoFrameWidth;

        // Get an instance of SWE Factory suitable to build components
        VideoCamHelper sweFactory = new VideoCamHelper();

        DataStream dataStream = sweFactory.newVideoOutputMJPEG("img", videoFrameWidth, videoFrameHeight);

        dataStruct = dataStream.getElementType();

        dataStruct.setLabel(PROCESS_OUTPUT_LABEL);

        dataStruct.setDescription(PROCESS_OUTPUT_DESCRIPTION);

        dataEncoding = dataStream.getEncoding();

        // obtain an event handler for this output
        String moduleID = parentProcess.getLocalID();

        String topic = getName();

        this.eventHandler = SensorHub.getInstance().getEventBus().registerProducer(moduleID, topic);

        logger.debug("Initialized");
    }

    @Override
    public void registerListener(IEventListener listener) {

        eventHandler.registerListener(listener);
    }

    @Override
    public void unregisterListener(IEventListener listener) {

        eventHandler.unregisterListener(listener);
    }
}
